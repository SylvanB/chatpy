from Message import Message
from socket import AF_INET, SOCK_STREAM, socket, SHUT_RDWR
from threading import Thread
import json
import sys
import time

class ChatServer:
    ''' A Multithreaded Chat Server

    ChatServer can handle multiple connections from multiple clients
    and broadcast them to other users connected to the server.

    address : string
            The ip address the server should bind to. E.g. `127.0.0.1`
    port : string
            The port the server should bind to.  E.g. `13337`
    serverIdentifier : string
            What the server should be identified as when making broadcasts such as connection/disconnection notifications.
    '''

    def __init__(self, address, port, serverIdentifier):      
        self.address = address
        self.port = port
        self.bufferSize = 10240
        self.serverIdentifier= serverIdentifier
        self.socket = socket(AF_INET, SOCK_STREAM)
        self.socket.bind((address, port))
        self.clients = []
        
    def Start(self, connectionQueueSize):
        ''' Starts the server to listen for client connections.

        The server will start listening for incoming connections, and create a new thread
        for these requests. 
    
        connectionQueueSize : int
                Defines the maximum amount of queued connections before addition connections
                should be dropped by the socket.
        '''
        print("Server Started at {} on port {}".format(self.address, self.port))
        self.socket.listen(connectionQueueSize)
        while True:
            clientSocket, _ = self.socket.accept()
            self.clients.append(clientSocket)
            Thread(target = self.HandleRequest, args = [clientSocket], daemon = True).start()

    def HandleRequest(self, client):
        ''' Listen to incoming messages from a client and broadcast them to other users

        Listen to incoming messages from a client and broadcast them to other users.
        Also handles disconnecting the user when an expected/unexpected disconnection occurs.

        client : Socket
               The socket that the new client has connected to. 
        '''
        try:
            self.Broadcast(list(filter(lambda x: x != client, self.clients)), "Connected to {} on port {} ".format(self.address, self.port), self.serverIdentifier)
            data = client.recv(self.bufferSize).decode()
            currentUser = data
            self.SendMotD(client)
            self.Broadcast([client], "{} has joined.".format(currentUser), self.serverIdentifier)

            while True:
                data = client.recv(self.bufferSize).decode()
                msg = Message().CreateFromData(data)
                if msg.message == "%quit%":
                    self.Broadcast([client], "{} has left the server".format(msg.user), self.serverIdentifier)
                    self.clients.remove(client)
                    client.close()
                    break
                else:
                    self.Broadcast([client], data)
        except:
            self.Broadcast([client], "{} has left the server".format(currentUser), self.serverIdentifier)
            self.clients.remove(client)
            client.shutdown(SHUT_RDWR)
            client.close()

    def Broadcast(self, clientExclusions, message, user = ""):
        ''' Send a message to all connected clients bar exclusions.

        This will send a message to all the connected users. This is used
        to ensure all clients recieve all messages from other users.   
            
        clientExclusions : String
                Can be used to define what users do not receive a broadcast.
        message : Message
                Message object to be propagated to all the users. 
        user : String 
                `user` is optional, as `Broadcast()` will default to checking for a user string value on the message object 
        '''

        if user != "":
            msg = Message().Create(user, message)
        else:
            msg = Message().CreateFromData(message)

        print("{} {}: {}".format(msg.timestamp, msg.user, msg.message))
        for client in self.clients:
            if client in clientExclusions:
                continue
            client.send(msg.toJSON().encode())


    def SendMotD(self, client):
        msg = "Message of the Day:\n♥*♡∞:｡.WELCOME｡.｡:∞♡*♥"
        self.Broadcast(list(filter(lambda x: x != client, self.clients)), msg, self.serverIdentifier)


def get_args(args):
    ''' Checks the program arguments are in the expected format'''
    if len(args) < 3:
        print("Invalid arguments provided")
        print("Expected format: python ChatServer.py <address> <port>")
        return None
    try:
        address = args[1]
        port = int(args[2])
        return (address, port)
    except:
        print("Invalid arguments provided")
        print("Expected format: python ChatServer.py <address> <port>")
        return None

if __name__ == "__main__":
    if (get_args(sys.argv) == None):
        sys.exit(0)
    address, port = get_args(sys.argv)
    server = ChatServer(address, port, "SERVER")
    server.Start(100)

    while True:
        time.sleep(0.5)