from Message import Message
from socket import socket, AF_INET, SOCK_STREAM,  SHUT_RDWR
from json import dumps
from threading import Thread
import tkinter as tk

class ChatClient:
    ''' A basic backend for the ChatUI class

    This class handles the socket operations for the ChatUI class.
    All data being sent and recieved will pass through this class
    '''

    def __init__(self, username, msgBox):
        self.socket = socket(AF_INET, SOCK_STREAM)
        self.username = username
        self.bufferSize = 10240
        self.msgBox = msgBox

    def Start(self, address, port):
        ''' Connect to the remote server and start listening for incoming messages
        
        Connect to the remote server and start listening for incoming messages
        on new thread, through `Recieve()`. 
        
        (address, port) : Tuple<string, string>
                This is passed to the socket in order to connect to the remote server
        '''
        try:
            self.socket.connect((address, port))
            Thread(target = self.Recieve, daemon = True).start()
            return True
        except:
            return False

    def Recieve(self):
        ''' Read a message from the server.
        
        This reads data from the socket and creates a message object from it, 
        which is then used to render the message properly within the frontend UI. 
        '''
        self.socket.send(self.username.encode())
        while True:                
            data = self.socket.recv(self.bufferSize).decode()
            msg = Message().CreateFromData(data) 
            self.msgBox.configure(state="normal")           
            self.msgBox.insert(tk.END, "{} {}: {}\n".format(msg.timestamp, msg.user, msg.message))
            self.msgBox.configure(state="disabled")

    def Send(self, msg):
        ''' Send a message to the remote server.

        Takes a message object and sends it to the remote server.

        msg : Message
                The message object to be sent to the remote server.
        '''
        data = msg.toJSON().encode()
        self.socket.sendall(data)

    def Shutdown(self):
        ''' Shuts down the client'''
        self.socket.shutdown(SHUT_RDWR)
        self.socket.close()
        
