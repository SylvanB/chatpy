import tkinter as tk
import tkinter.messagebox as tkMessageBox
from ChatClient import ChatClient
from Message import Message
import sys

class ChatUi:
    ''' A TkInter based Chat Application

    ChatUI acts as a front-end for the ChatClient class
    This will generate a new TkInter window with serveral widgets
    used to provide a user with input fields message boxes and buttons
    for the chat functionality.    
    '''

    def __init__(self):
        
        self.root = tk.Tk()
        self.root.title("PyChat")
        self.root.protocol("WM_DELETE_WINDOW", self.CloseWindow)
        self.root.resizable(width=False, height=False)

        self.usernameInputLabel = tk.Label(self.root, text="Username", font=("bold", 10))
        self.username = tk.StringVar()
        self.usernameInput = tk.Entry(self.root, text=self.username)

        self.serverAddressInputLabel = tk.Label(self.root, text="Server Address", font=("bold", 10))
        self.serverAddress = tk.StringVar()
        self.serverAddressInput = tk.Entry(self.root, text=self.serverAddress)

        self.serverPortInputLabel = tk.Label(self.root, text="Server Port", font=("bold", 10))
        self.serverPort = tk.IntVar()
        self.serverPortInput = tk.Entry(self.root, text=self.serverPort)

        self.connectButton = tk.Button(self.root, text="Connect", command=self.Connect)
        self.disconnectButton = tk.Button(self.root, text="Disconnect", command=self.Disconnect)

        self.messageBox = tk.Text(self.root, state="disabled")

        self.userInput = tk.StringVar()
        self.inputField = tk.Entry(self.root, text=self.userInput)
        self.inputField.bind("<Return>", self.Send)

        self.usernameInputLabel.grid(row=0, column=0, padx=5, sticky=tk.E)
        self.usernameInput.grid(row=0, column=1)

        self.serverAddressInputLabel.grid(row=0, column=2, padx=5, sticky=tk.E)
        self.serverAddressInput.grid(row=0, column=3)

        self.serverPortInputLabel.grid(row=0, column=4, padx=5, sticky=tk.E)
        self.serverPortInput.grid(row=0, column=5)

        self.connectButton.grid(row=0, column=6, padx=5, pady=5)

        self.messageBox.grid(row=1, column=0, columnspan=7)

        self.inputField.grid(row=2, column=0, columnspan=7, pady=5, padx=5, sticky=tk.W + tk.E)

    def Connect(self):
        ''' Connects to a remote chat server.

        This is a Callback function registered on the Connect button, will initiate a 
        connection to the remote server and enable a user to start sending messages.
        This will also hide the Username, Server Address, Server Port fields 
        and the Connect button.
        '''
        self.client = ChatClient(self.username.get(), self.messageBox)
        hasConnected = self.client.Start(self.serverAddress.get(), self.serverPort.get())
        if hasConnected:
            self.connectButton.grid_forget()
            self.usernameInput.grid_forget()
            self.usernameInputLabel.grid_forget()
            self.serverAddressInput.grid_forget()
            self.serverAddressInputLabel.grid_forget()
            self.serverPortInput.grid_forget()
            self.serverPortInputLabel.grid_forget()

            self.disconnectButton.grid(row=0, column=7, padx=5, pady=5)
        else:
            self.messageBox.configure(state="normal")
            self.messageBox.insert(tk.END,"Failed to connect to server.\n")
            self.messageBox.configure(state="disabled")


    def Disconnect(self):
        ''' Disconnects the client from the server.

        Callback function registered on the Connect button. 
        Terminates the connection to the remote server, hides 
        the Disconnect button and shows the Username, Server Address, 
        Server Port fields and the Connect button.
        '''

        self.client.Shutdown()
        self.disconnectButton.grid_forget() # hello world

        self.usernameInputLabel.grid(row=0, column=0, padx=5, sticky=tk.E)
        self.usernameInput.grid(row=0, column=1)

        self.serverAddressInputLabel.grid(row=0, column=2, padx=5, sticky=tk.E)
        self.serverAddressInput.grid(row=0, column=3)

        self.serverPortInputLabel.grid(row=0, column=4, padx=5, sticky=tk.E)
        self.serverPortInput.grid(row=0, column=5)

        self.connectButton.grid(row=0, column=6, padx=5, pady=5)
        

    def Send(self, event):
        ''' Send a message to the remote server.

        Callback function called when the user presses the `Enter` key 
        when entering a message. This function fetches all the required
        information for the message from the interface widgets and constructs
        a message ready to be sent by the ChatClient object.
        '''
        message = self.userInput.get()
        username = self.username.get()
        msg = Message().Create(username, message)
        self.ParseServerCommands(message)

        self.client.Send(msg)
        self.messageBox.configure(state="normal")
        self.messageBox.insert(tk.END,"{} {}: {}\n".format(msg.timestamp, msg.user, msg.message))
        self.messageBox.configure(state="disabled")
        self.userInput.set("")

    def ParseServerCommands(self, message):
        ''' Checks a message for server commands

        Called from `Send()` to check if the message the user entered was a 
        server command and if this command requires any additional actions from
        the client.

        message : Message
                The message being checked for server commands
        '''
        if message in ("/quit", "/disconnect"):
            self.Disconnect()
            

    def CloseWindow(self):
        ''' Closes the program. 

        Callback function called when a user clicks the 'X' in the top right
        of the program. It will show a confirmation dialog and after the user
        has confirmed the program will be closed.
        '''
        if tkMessageBox.askokcancel('Quit', 'Are you sure you want to exit?'):
            self.root.destroy()
            self.client.Shutdown()
            sys.exit(0)


# Create the main window
program = ChatUi()


# Run forever!
program.root.mainloop()
