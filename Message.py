import json
import datetime

class Message:
    ''' Message format used by ChatClient and ChatServer

    Message can be used to facilitate communication in a commonly readable format
    across both the server and the client applications.
    '''
    def CreateFromData(self, data):
        try:            
            data = json.loads(data)
            user = data["user"]
            msg = data["message"]
            timestamp = data["timestamp"]
            self.user = user
            self.message = msg
            self.timestamp = timestamp
            return self
        except:
            return None

    def Create(self, user, message):
        self.user = user
        self.message = message
        self.timestamp = datetime.datetime.now().strftime("[%x %X]")
        return self

    def toJSON(self):
        return json.dumps(self, 
                        default=lambda o: o.__dict__, 
                        sort_keys=True, indent=4)